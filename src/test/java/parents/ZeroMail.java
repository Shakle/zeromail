package parents;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

import org.apache.commons.io.FileUtils;
import org.junit.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import static general.GeneralMethods.driver;
import static general.GeneralMethods.log;

public class ZeroMail {

    @BeforeClass
    public static void SetUpClass() {

    }

    @Before
    public void SetUP() throws AWTException, IOException {

        if (driver == null) {

            System.setProperty("webdriver.chrome.driver",
                    "C:/Users/qalit/IdeaProjects/ZeroMail/src/main/resources/drivers/chromedriver.exe");

            Map<String, Object> chromeOptions = new HashMap<String, Object>();
            chromeOptions.put("binary", "C:/zeromail/zeromail-desktop.exe");
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("chromeOptions", chromeOptions);

            driver = new ChromeDriver(capabilities);

            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        }
    }
}