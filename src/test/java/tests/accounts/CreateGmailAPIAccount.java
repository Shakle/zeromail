package tests.accounts;

import org.junit.Test;
import parents.ZeroMail;

import java.io.IOException;

import static general.GeneralMethods.*;
import static pages.Login.*;
import static pages.Main.*;


public class CreateGmailAPIAccount extends ZeroMail {

    @Test

    public void CreateGmailAPIAccount() throws ClassNotFoundException, IOException, InstantiationException, IllegalAccessException, InterruptedException {

        saveCurrentWindowHandle();
        clickGmailAccountButton();
        switchToTheNewWindowHandle();
        inputGmailAddress();
        clickGmailNextButton();
        inputGmailPassword();
        clickGmailEnterButton();
        Thread.sleep(4000);
        clickGmailAllowButton();
        switchToPreviousWindowHandle();
        checkPresenceOnMainPageViaAccountName();
    }
}
