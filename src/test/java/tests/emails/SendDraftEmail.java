package tests.emails;

import org.junit.Test;

import java.io.IOException;

import static pages.Folders.*;
import static pages.Main.*;
import static pages.Compose.*;
import static pages.Email.*;

public class SendDraftEmail {

    @Test

    public void SendDraftEmail() throws ClassNotFoundException, IOException, InstantiationException, IllegalAccessException {

        clickDraftsFolder();
        clickTestEmail();
        clickSendButton();
        checkDraftSentNotification();
        clickOutboxFolder();
        checkEmailInOutboxFolder();
        clickSentFolder();
        checkEmailInSentFolder();
    }
}