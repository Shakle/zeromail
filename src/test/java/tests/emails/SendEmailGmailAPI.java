package tests.emails;

import org.junit.Test;
import pages.Folders;
import parents.ZeroMail;

import java.io.IOException;

import static pages.Folders.*;
import static pages.Main.*;
import static pages.Compose.*;

public class SendEmailGmailAPI extends ZeroMail {

    @Test

    public void SendEmailGmailAPI () throws ClassNotFoundException, IOException, InstantiationException, IllegalAccessException, InterruptedException {

        clickComposeButton();
        inputEmailRecipient();
        inputEmailSubject();
        inputEmailText();
        clickSendButton();
        checkMessageSentNotification();
        clickOpenCloseFolderIcon();
        clickOutboxFolder();
        checkEmailInOutboxFolder();
        clickSentFolder();
        checkEmailInSentFolder();
    }
}
