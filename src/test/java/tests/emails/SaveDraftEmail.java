package tests.emails;

import org.junit.Test;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;

import static pages.Folders.*;
import static pages.Main.*;
import static pages.Compose.*;
import java.awt.Robot;

public class SaveDraftEmail {

    @Test

    public void SaveDraftEmail() throws ClassNotFoundException, IOException, InstantiationException, IllegalAccessException, AWTException, InterruptedException {

        Robot robot = new Robot();

        clickComposeButton();
        inputEmailRecipient();
        inputEmailSubject();
        inputEmailText();
        clickComposeCloseButton();
        Thread.sleep(1000);
        robot.keyPress(KeyEvent.VK_TAB);
        robot.keyRelease(KeyEvent.VK_TAB);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        clickDraftsFolder();
        checkEmailInDraftsFolder();
    }
}
