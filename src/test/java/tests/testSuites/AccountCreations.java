package tests.testSuites;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.accounts.CreateGmailAPIAccount;
import tests.emails.SaveDraftEmail;
import tests.emails.SendDraftEmail;
import tests.emails.SendEmailGmailAPI;


import java.io.File;
import java.io.IOException;

import static general.GeneralMethods.log;


@RunWith(Suite.class)

@Suite.SuiteClasses({
        CreateGmailAPIAccount.class,
        SendEmailGmailAPI.class,
        SaveDraftEmail.class,
        SendDraftEmail.class
})


public class AccountCreations {

    @BeforeClass

    public static void BeforeStart(){

        try {
            FileUtils.cleanDirectory(new File("C:\\Users\\qalit\\AppData\\Roaming\\zeromail-desktop"));
            log.info("Zeromail cache has been cleared");
        } catch (Exception e){
            log.info("ZeroMail cache was empty");
        }
    }

    @AfterClass

    public static void Ending() throws IOException {

        Runtime.getRuntime().exec("taskkill /f /im zeromail-desktop.exe");
    }
}
