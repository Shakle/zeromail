package pages;

import java.io.IOException;

import static general.ConfigData.ui;
import static general.GeneralMethods.checkElementPresence;
import static general.GeneralMethods.clickElement;

public class Folders {

    public static void clickOpenCloseFolderIcon() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        clickElement(ui("Folders.OpenClose.Icon"), "Open/Close icon");
    }

    /* Outbox Folder */

    public static void checkEmailInOutboxFolder() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        checkElementPresence(ui("Folders.MailList.Subject"), "Previously sent message in outbox folder");
    }
    public static void clickOutboxFolder() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        clickElement(ui("Folders.Outbox"), "Outbox folder");
    }

    /* Sent Folder */

    public static void checkEmailInSentFolder() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        checkElementPresence(ui("Folders.MailList.Subject"), "Previously sent message in sent folder");
    }

    public static void clickSentFolder() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        clickElement(ui("Folders.Sent"), "Sent folder");
    }

    /* Drafts Folder */

    public static void clickDraftsFolder() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        clickElement(ui("Folders.Drafts"), "Drafts folder");
    }

    public static void checkEmailInDraftsFolder() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        checkElementPresence(ui("Folders.MailList.Subject"), "Saved message in drafts folder");
    }
}
