package pages;

import general.GeneralMethods;
import org.openqa.selenium.By;

import java.io.IOException;

import static general.ConfigData.ui;
import static general.GeneralMethods.clickElement;
import static general.GeneralMethods.driver;
import static general.GeneralMethods.inputText;

public class Compose {

    public static void inputEmailText() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        inputText(ui("Email.Input.Text"), "This is a test email", "Text field");
    }

    public static void inputEmailSubject() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        inputText(ui("Email.Input.Subject"), "Test Email", "Subject");
    }

    public static void inputEmailRecipient() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        inputText(ui("Email.Input.Recipient"), "qalits@outlook.com", "Recipient");
    }

    public static void clickSendButton() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        clickElement(ui("Email.Click.Submit"), "Send button");
    }

    public static void clickComposeCloseButton() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        clickElement(ui("Compose.Close.Button"), "Composer close button");
    }

}