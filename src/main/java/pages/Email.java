package pages;

import java.io.IOException;

import static general.ConfigData.ui;
import static general.GeneralMethods.*;

public class Email {

    public static void clickTestEmail() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        clickElement(ui("Folders.MailList.Subject"), "Test Email");
    }
}
