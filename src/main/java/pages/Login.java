package pages;

import java.io.IOException;
import static general.ConfigData.ui;
import static general.GeneralMethods.*;

public class Login {

    public static void clickGmailAccountButton() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        clickElement(ui("Gmail.Login.GmailButton"), "Gmail account");
    }

    public static void inputGmailAddress() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        inputText(ui("Gmail.Login.AdressInput"), "dmitry.andrianov@litslink.com", "email address field");
    }

    public static void inputGmailPassword() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
            inputText(ui("Gmail.Login.PaswordInput"), "sPs12345","Password field");
    }

    public static void clickGmailNextButton() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        clickElement(ui("Gmail.Login.NextButton"), "Next button");
    }

    public static void clickGmailEnterButton() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        clickElement(ui("Gmail.Login.EnterButton"), "Enter button");
    }

    public static void clickGmailAllowButton() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        clickElement(ui("Gmail.Login.AllowButton"), "Gmail 'allow' button");
    }
}
