package pages;

import general.GeneralMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

import static general.ConfigData.ui;
import static general.GeneralMethods.*;

public class Main {

    public static void checkPresenceOnMainPageViaAccountName() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        driver.findElement(ui("Main.Accounts.Qalits"));
        log.info("Account has been found");
    }

    public static void clickComposeButton() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        clickElement(ui("Main.Compose.Button"), "Compose button");
    }

    public static void checkMessageSentNotification() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        checkElementPresence(ui("Main.Notification.MessageSent"), "Message sent notification");
    }

    public static void checkDraftSentNotification() throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        checkElementPresence(ui("Main.Notification.DraftSent"), "Draft sent notification");
    }
}
