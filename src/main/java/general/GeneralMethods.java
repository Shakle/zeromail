package general;

import org.apache.log4j.Logger;
import org.apache.xpath.XPath;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GeneralMethods {

    public static WebDriver driver;
    public static Logger log = Logger.getLogger(GeneralMethods.class.getName());

    private static String mainWindowHandle;

    //Clicks on an element with UI mapping xpath key
    public static void clickElement(By XPath, String ClickText) {
        driver.findElement(XPath).click();
        log.info(ClickText + " has been clicked");
    }

    //Checks if an element is present on the page
    public static void checkElementPresence(By XPath, String ClickText) {
        driver.findElement(XPath);
        log.info(ClickText + " presence on page confirmed");
    }

    //Inputs some text into field
    public static void inputText(By XPath, String text, String ClickText) {
        driver.findElement(XPath).sendKeys(" ");
        driver.findElement(XPath).clear();
        driver.findElement(XPath).sendKeys(text);
        log.info(text + " has been written into " + ClickText);
    }

    /* Window handle actions */

    public static void saveCurrentWindowHandle(){
        mainWindowHandle = driver.getWindowHandle();
        Set<String> allHandles = driver.getWindowHandles();
    }

    public static void switchToTheNewWindowHandle() throws InterruptedException {
        Thread.sleep(3000);
        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }
    }

    public static void switchToPreviousWindowHandle() {
        driver.switchTo().window(mainWindowHandle);
    }
}
